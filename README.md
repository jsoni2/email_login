# PoC Project - Backend 

PoC User Authentication with Email address as username, Using Django, Django REST framework and MySQL.

* User can register account with email.
* User can login with email and password.
* gets authentication token to maintain session and other HTTP requests to server.
* only authorized user can get RACI data in JSON format.
* After Login, Current User get his/her own account details.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

You need to have Python and MySQL installed on your machine.

```
$python -V
Python 3.6.5

$mysql -V
mysql  Ver 8.0.11 for Win64 on x86_64 (MySQL Community Server - GPL)
```

Also, Please Edit Database Configuratin in settings.py file according to your own MySQL configuration.

### Set-Up
Create Virtual Environment with venv.

Python installers for Windows include pip. You should be able to access pip using:
```
$ py -m pip --version
```
You can make sure that pip is up-to-date by running:
```
$ py -m pip install --upgrade pip
```

Installing virtualenv:
```
$ py -m pip install --user virtualenv
```
Creating a virtualenv:
```
$ py -m virtualenv env
```
Activating a virtualenv
```
$ .\env\Scripts\activate
```

Or Follow the link below to set-up virtual environment
(https://packaging.python.org/guides/installing-using-pip-and-virtualenv/)


### Installing Dependencies
Go to root folder of project.
After activating virtualenv you should be able to see (env) prefixed on you command line, this indicates you are working in virtual environment.
Install Dependencies from requirements.txt file

```
(env) $ pip install -r requeriments.txt
```


### Running the app

cd into email_login folder of project with your favourite command line tool and run following commands

```

(env) $ python manage.py makemigrations

(env) $ python manage.py migrate

(env) $ python manage.py runserver
```

Go to (http://localhost:8000/)

click signup, register, login with same credential to sign-in

To Test with httpie follow commands from screenshot below:
![photos 7_18_2018 12_12_34 pm](https://user-images.githubusercontent.com/18238847/42902562-f2d50512-8a83-11e8-8adb-75012443f8b1.png)

Test Raci API with Authorization Token in request header. 
![image](https://user-images.githubusercontent.com/18238847/43108554-3cc0e594-8e97-11e8-926e-bf798a1d062e.png)

API Endpoint to Get User Details
![postman 7_24_2018 4_44_27 pm](https://user-images.githubusercontent.com/18238847/43171880-2e296fae-8f61-11e8-9375-a36582bb3a58.png)

