from rest_framework import generics

from .models import Raci
from .serializers import RaciSerializer
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated

class RaciList(generics.ListAPIView):
    queryset = Raci.objects.all()
    serializer_class = RaciSerializer
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

class RaciDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Raci.objects.all()
    serializer_class = RaciSerializer

class RaciCreate(generics.CreateAPIView):
    queryset = Raci.objects.all()
    serializer_class = RaciSerializer
