from rest_framework import serializers
from . import models

class RaciSerializer(serializers.ModelSerializer):
    """docstring for PostSerializer."""
    class Meta:
        fields = ('id', 'Group','Activity','Developer','Agile_Project_Owner','Scrum_Master','SDET','TechLead','TechnicalPO','Architect','Automation',)
        model = models.Raci
