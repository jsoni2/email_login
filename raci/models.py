from django.db import models

# Create your models here.

class Raci(models.Model):
    Group = models.CharField(max_length=50)
    Activity = models.CharField(max_length=50)
    Developer = models.CharField(max_length=50)
    Agile_Project_Owner = models.CharField(max_length=50)
    Scrum_Master = models.CharField(max_length=50)
    SDET = models.CharField(max_length=50)
    TechLead = models.CharField(max_length=50)
    TechnicalPO = models.CharField(max_length=50)
    Architect = models.CharField(max_length=50)
    Automation = models.CharField(max_length=50)

    def __str__(self):
        return self.title
