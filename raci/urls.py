from django.urls import path

from . import views

urlpatterns = [
    path('raci/', views.RaciList.as_view()),
    path('raci/<int:pk>/', views.RaciCreate.as_view()),
]
