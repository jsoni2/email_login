from django.apps import AppConfig


class NormalizedRaciConfig(AppConfig):
    name = 'normalized_raci'
