from rest_framework import serializers
from . import models

class RACISerializer(serializers.ModelSerializer):
    """docstring for PostSerializer."""
    class Meta:
        fields = ('id', 'Group','Activity','Developer','Agile_Project_Owner','Scrum_Master','SDET','TechLead','TechnicalPO','Architect','Automation',)
        model = models.RaciNormalized

class GroupSerializer(serializers.ModelSerializer):
    """docstring for PostSerializer."""
    class Meta:
        fields = ('id', 'GroupName',)
        model = models.Group

class ActivitySerializer(serializers.ModelSerializer):
    """docstring for PostSerializer."""
    class Meta:
        fields = ('id', 'ActivityName',)
        model = models.Activity

class SymobolSerializer(serializers.ModelSerializer):
    """docstring for PostSerializer."""
    class Meta:
        fields = ('id', 'RACISymbol',)
        model = models.RACIAnnotation
