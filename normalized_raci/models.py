from django.db import models

class Group(models.Model):
    GroupName = models.CharField(max_length=50)

class Activity(models.Model):
    ActivityName = models.CharField(max_length=50)

class RACIAnnotation(models.Model):
    RACISymbol = models.CharField(max_length=50)

class RaciNormalized(models.Model):
    Group = models.ForeignKey(Group, related_name='raci_group', on_delete=models.CASCADE)
    Activity = models.ForeignKey(Activity,related_name='raci_activity', on_delete=models.CASCADE)
    Developer = models.ForeignKey(RACIAnnotation,related_name='raci_dev_annotation', on_delete=models.CASCADE)
    Agile_Project_Owner = models.ForeignKey(RACIAnnotation,related_name='raci_agileprojectowner_annotation', on_delete=models.CASCADE)
    Scrum_Master = models.ForeignKey(RACIAnnotation,related_name='raci_scrummaster_annotation', on_delete=models.CASCADE)
    SDET = models.ForeignKey(RACIAnnotation,related_name='raci_sdet_annotation', on_delete=models.CASCADE)
    TechLead = models.ForeignKey(RACIAnnotation,related_name='raci_techlead_annotation', on_delete=models.CASCADE)
    TechnicalPO = models.ForeignKey(RACIAnnotation,related_name='raci_technicalpo_annotation', on_delete=models.CASCADE)
    Architect = models.ForeignKey(RACIAnnotation,related_name='raci_architect_annotation', on_delete=models.CASCADE)
    Automation = models.ForeignKey(RACIAnnotation,related_name='raci_automation_annotation', on_delete=models.CASCADE)
    class Meta:
        unique_together = (('Group', 'Activity', 'Developer', 'Agile_Project_Owner', 'Scrum_Master', 'SDET', 'TechLead','TechnicalPO','Architect','Automation'),)
        index_together = (('Group', 'Activity', 'Developer', 'Agile_Project_Owner', 'Scrum_Master', 'SDET', 'TechLead','TechnicalPO','Architect','Automation'),)
