from rest_framework import generics

from .models import RaciNormalized, Activity, Group, RACIAnnotation
from .serializers import RACISerializer, GroupSerializer, ActivitySerializer, SymobolSerializer
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated

class RaciList(generics.ListAPIView):
    queryset = RaciNormalized.objects.all()
    serializer_class = RACISerializer
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)


class RaciCreate(generics.CreateAPIView):
    queryset = RaciNormalized.objects.all()
    serializer_class = RACISerializer

class RaciGroupList(generics.ListAPIView):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

class RaciGroupCreate(generics.CreateAPIView):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

class RaciActivityList(generics.ListAPIView):
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer

class RaciActivityCreate(generics.CreateAPIView):
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer

class RaciSymbolList(generics.ListAPIView):
    queryset = RACIAnnotation.objects.all()
    serializer_class = SymobolSerializer

class RaciSymbolCreate(generics.CreateAPIView):
    queryset = RACIAnnotation.objects.all()
    serializer_class = SymobolSerializer
