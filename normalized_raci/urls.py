from django.urls import path

from . import views

urlpatterns = [
    path('raci/', views.RaciList.as_view()),
    path('raci/<int:pk>/', views.RaciCreate.as_view()),
    path('raci/groups/', views.RaciGroupList.as_view()),
    path('raci/groups/<int:pk>/', views.RaciGroupCreate.as_view()),
    path('raci/activity/', views.RaciActivityList.as_view()),
    path('raci/activity/<int:pk>/', views.RaciActivityCreate.as_view()),
    path('raci/symbol/', views.RaciSymbolList.as_view()),
    path('raci/symbol/<int:pk>/', views.RaciSymbolCreate.as_view()),
]
