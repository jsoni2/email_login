# djauth/urls.py
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include('api.urls')),
    path('api/v1/', include('raci.urls')),
    path('api/v1/', include('userapi.urls')),
    path('api/v2/', include('normalized_raci.urls')),
]
