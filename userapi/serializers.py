from rest_framework import serializers
from .import models

class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'first_name', 'last_name', 'phone_number',)
        model = models.User
