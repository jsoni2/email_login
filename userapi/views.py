from rest_framework import generics

# from rest_framework.views import APIView
from .models import User
from .serializers import UserDetailSerializer
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

class UserDetail(generics.RetrieveAPIView):
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    model = User
    serializer_class = UserDetailSerializer
    def get(self, request, format=None):
        # o = {'user_email': '{0}'.format(request.user)}
        return Response(UserDetailSerializer(request.user).data)
